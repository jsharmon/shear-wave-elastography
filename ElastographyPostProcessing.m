%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Updated: Jonah Harmon, 12/11/2019

% DESCRIPTION

% Post-processing for shear wave elastography data.

% Adapted from:

%   Nordenfur, T. Comparison of pushing sequences for shear wave
%   elastography. Masters Thesis. KTH Royal Institute of Technology.
%   Stockholm, Sweden. 2013.

%   Song, P. et al. Comb-push Ultrasound Shear Elastography (CUSE): A Novel
%   Method for Two-dimensional Shear Elasticity Imaging of Soft Tissues.
%   IEEE Trans Med Imaging 2012 Sep; 31(9): 1821�1832.

% As well as code for the Loupas autocorrelator from John Pitre.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all

%% User settings
% Name of file to process
elastFilename = './TestData/CombPush-PVA.mat';

% Cropping data in time
frameMin = 4; frameMax = 80;

% Settings for shear wave velocity estimation
% do_demean: whether to remove the mean from each time series (default:1)
% do_window: whether to Hanning window each time series (default:1)
% do_lowpass: whether to low-pass Butterworth filter each time series (default:0)
% dlat: lateral spacing; the two points to compare are spaced by 2*dlat (default:4)
% interp_f: interpolation factor for sub-sample cross-correlation (default:5)
settings = struct('do_demean', 1, ...
                  'do_window', 1, ...
                  'do_lowpass', 1, ...
                  'dlat', 4, ...
                  'interp_f', 5, ...
                  'window_ratio', 0.5, ...   % Related to windowing
                  'lowpass_order', 3, ...    % Param for making filter
                  'lowpass_cutoff', 0.7);    % Param for making filter
              
% Whether or not to filter the separated directional axial velocities with
% a 2D median filter ([5, 5]) to smooth noise before estimating shear wave
% velocity. 1 = filter, 0 = no filter.
filterAxVel = 1;  

% Specify whether or not to median filter result before display. Default to
% 1. If set to 0, outliers typically swamp display
settings.filterResult = 1;

% For final display, the left side of the right-to-left shear wave velocity 
% estimation and the right side of the left-to-right estimation are
% selected, and the center values are averaged. This value sets the
% percentage of the image on either side that is directly pulled from one
% image or the other. For example, a value of 0.2 indicated that the left
% 20% of the image is straight from RL, the right 20% is from LR, and the
% middle 60% is averaged between the two.
cutPct = 0.3;

% Decide whether to scale from pixel delay to m/s; this can severely
% exacerbate outliers. As such, default to 0.
scaleToVelocity = 0;

%% Load in and prepare IQ data
% Navigate to appropriate folder
cd C:\Users\verasonics\jonah\Code\shear-wave-elastography

% Notify user
disp('Preparing IQ Data...')

% Read in datafile, generate single complex IQ matrix. Angle compounding
% was conducted by VSX immediately after data acquisition, if relevant
load(elastFilename);

% Generate complex matrix, grab intensity frame for display later
IQ = complex(IData, QData);
BMode = sqrt(sqrt(IData(:,:,5).^2 + QData(:,:,5).^2));

% Display BMode, allow user to specify ROI
imagesc(BMode); ROI = imrect; pos = getPosition(ROI);
axMin = ceil(pos(2)); axMax = ceil(pos(2)+pos(4)); 
latMin = floor(pos(1)); latMax = floor(pos(1)+pos(3));

% Crop IQ data based on user specified ROI
% IQ = IQ(axMin:axMax, latMin:latMax, frameMin:frameMax);
IQ = IQ(axMin:axMax, :, frameMin:frameMax);

clear IData QData elastFilename frame
close gcf

%% Local axial velocity estimation, Loupas' 2D autocorrelator
% Notify user
disp('Starting autocorrelation...')

% Window sizes for 2D autocorrelation - 2D in the sense of axial and page
% dimensions.
M = 12;  % Range gate length (in the axial direction)
N = 10;  % Ensemble length (over pages)

% Compute and assign constants
% Speed of sound, c, already assigned
fc = freq;                                     % Transducer center frequency
ts = 1/(rcv.decimSampleRate*1e6);              % Sampling interval
Ts = 1/swe.imaging_prf;                        % Pulse repetition interval
fdem = rcv.demodFrequency/rcv.decimSampleRate; % Normalized demod frequency

% Loupas velocity estimate - code from John's script
% Using similar notation to Nordenfur, 2013.
% UU denotes the numerator of the numerator term (up-up)
% UD denotes the denominator of the numerator term (up-down)
% DU denotes the numerator of the denominator term (down-up)
% DD denotes the denominator of the denominator term (down-down)
% Prefixes of SS denote the double summations over axial and page indices
v = zeros(size(IQ) - [M, 0, N]);
IQ = permute(IQ, [1, 3, 2]);
for axi = 1:size(v, 1)
    axial_range = axi:(axi + M - 1);
    for page = 1:size(v, 3)
        ensemble = page:(page + N - 1);
        I = real(IQ(axial_range, ensemble, :));
        Q = imag(IQ(axial_range, ensemble, :));
        UU = Q(1:M,1:N-1,:).*I(1:M,2:N,:) - I(1:M,1:N-1,:).*Q(1:M,2:N,:);
        UD = I(1:M,1:N-1,:).*I(1:M,2:N,:) + Q(1:M,1:N-1,:).*Q(1:M,2:N,:);
        DU = Q(1:M-1,1:N,:).*I(2:M,1:N,:) - I(1:M-1,1:N,:).*Q(2:M,1:N,:);
        DD = I(1:M-1,1:N,:).*I(2:M,1:N,:) + Q(1:M-1,1:N,:).*Q(2:M,1:N,:);
        SSUU = sum(sum(UU, 1), 2);
        SSUD = sum(sum(UD, 1), 2);
        SSDU = sum(sum(DU, 1), 2);
        SSDD = sum(sum(DD, 1), 2);
        v(axi,:,page) = (c/2)*(ts/Ts)*atan(SSUU./SSUD)./(2*pi*fdem + atan(SSDU./SSDD));
    end
    
    % Notify user of progress
    if mod(axi, 25) == 0
        fprintf('%03d axial locations processed...\n', axi)
    end
end

clear DD DU I Q IQ SSDD SSDU SSUD SSUU UD UU

% Grab frame for display
sliceDepth = ceil(size(v, 1)/2);  % Halfway down
velExUnfiltered = squeeze(v(sliceDepth,:,:));

%% Directional filtering
% Filter to keep shear waves traveling only in one direction; keep both,
% process separately, average after SWV estimation.
axVelLR = zeros(size(v));
axVelRL = zeros(size(v));

% Notify user
disp('Directional filtering...')

% Loop through all slices, filter
for i = 1:size(v, 1)
    % Grab slice
    slice = v(i,:,:);

    % Transform slice to Fourier domain
    slice = squeeze(slice);
    fourierSlice = fft2(slice);

    % Generate mask for use on all frames
    if i == 1
        % Generate mask to select a single wave propagation direction
        mask1 = ones(size(fourierSlice));
        halfwayCols = size(fourierSlice,2)/2; halfwayRows = size(fourierSlice,1)/2;

        % Set zeros for filtering mask
        mask1(1:floor(halfwayRows), 1:floor(halfwayCols)) = 0;
        mask1(ceil(halfwayRows)+1:end, ceil(halfwayCols)+1:end) = 0;

        % Generate mask to capture reverse propagation direction
        mask2 = flipud(mask1);
    end

    % Filter slice in Fourier domain, invert back to spatiotemporal
    filteredFourierSlice1 = mask1 .* fourierSlice;
    filteredSlice1 = real(ifft2(filteredFourierSlice1));
    
    % Filter based on user setting
    if filterAxVel == 1
        axVelLR(i,:,:) = medfilt2(filteredSlice1, [5,5]);
    else
        axVelLR(i,:,:) = filteredSlice1;
    end

    % Repeat with opposite direction
    filteredFourierSlice2 = mask2 .* fourierSlice;
    filteredSlice2 = real(ifft2(filteredFourierSlice2));
    
    % Grab frames
    if i == sliceDepth
        spectimRaw = fourierSlice;
        spectIm1 = filteredFourierSlice1;
        spectIm2 = filteredFourierSlice2;
    end
    
    % Filter based on user setting
    if filterAxVel == 1
        axVelRL(i,:,:) = medfilt2(filteredSlice2, [5,5]);
    else
        axVelRL(i,:,:) = filteredSlice2;
    end
    
    vFilt(i,:,:)=medfilt2(slice, [5,5]);
end

%clear filterSelect v slice fourierSlice mask halfwayCols halfwayRows ...
%      filteredFourierSlice1 filteredSlice1 filteredFourierSlice2 ...
%      filteredSlice2
  
% Grab frames for display
velExFilteredLR = squeeze(axVelLR(sliceDepth,:,:));
velExFilteredRL = squeeze(axVelRL(sliceDepth,:,:));
  
%% Shear wave velocity estimation and mapping
% Calculate butterworth filter coefficients
[settings.lowpass_B, settings.lowpass_A] = ...
    butter(settings.lowpass_order, settings.lowpass_cutoff, 'low');

% Set dimensions of axial velocity matrix to (frame, axial, lateral)
axVelLR = permute(axVelLR, [3 1 2]);
axVelRL = permute(axVelRL, [3 1 2]);

% Notify user
disp('Shear wave velocity estimation...')

% Estimate shear wave velocity for each specified pixel
swVelLR = NaN(size(axVelLR,2), size(axVelLR,3)-2*settings.dlat);
swVelRL = NaN(size(axVelRL,2), size(axVelRL,3)-2*settings.dlat);
for ax = 1:size(axVelLR,2)
    for lat = 1+settings.dlat:size(axVelLR,3)-settings.dlat
        swVelLR(ax,lat-settings.dlat) = estimateLocalShearWaveVelocity(axVelLR,ax,lat,'LR',settings);
        swVelRL(ax,lat-settings.dlat) = estimateLocalShearWaveVelocity(axVelRL,ax,lat,'RL',settings);
    end
end

% Median filter to smooth spikes
if settings.filterResult == 1
    swVelLR = medfilt2(swVelLR, [3, 3]);
    swVelRL = medfilt2(swVelRL, [3, 3]);
end

% Combine LR and RL according to cutPct setting
sz = size(swVelLR);
chunk = round(sz(2)*cutPct);
avgSWVel = zeros(sz(1), sz(2));
avgSWVel(:, 1:chunk) = ...
    swVelRL(:, 1:chunk);                       % Grab left side from RL
avgSWVel(:, sz(2)-chunk:sz(2)) = ...
    swVelLR(:, sz(2)-chunk:sz(2));             % Grab right side from LR
avgSWVel(:, 1+chunk:sz(2)-chunk-1) = ...
    (swVelLR(:, 1+chunk:sz(2)-chunk-1) + ...
     swVelRL(:, 1+chunk:sz(2)-chunk-1)) / 2;   % Average center

clear axVelLR axVelRL

%% Display result
% Notify user
disp('Plotting...')

figure('Position', [0 150 900 900])

% B-mode, show ROI
subplot(3, 2, 1)
imagesc(BMode), colormap(gca,'gray')
rectangle('Position', [latMin, axMin, latMax-latMin, axMax-axMin], 'EdgeColor', 'r') 
title('B-mode')

% Axial velocity, unfiltered
subplot(3, 2, 2)
imagesc(velExUnfiltered), colorbar, colormap default
title(sprintf('AxVel, raw (%03d depth)', sliceDepth))

% Axial velocity, filtered, LR
subplot(3, 2, 3)
imagesc(velExFilteredLR), colorbar
title(sprintf('AxVel, filtered, LR (%03d depth)', sliceDepth))

% Axial velocity, filtered, RL
subplot(3, 2, 4)
imagesc(velExFilteredRL), colorbar
title(sprintf('AxVel, filtered, RL (%03d depth)', sliceDepth))

% Shear wave velocity map, single direction
subplot(3, 2, 5)
imagesc(swVelLR), colorbar
title('Shear wave velocity map, LR')

% Shear wave velocity map, RL
subplot(3, 2, 6)
imagesc(swVelRL), colorbar
title('Shear wave velocity map, RL')

%% Display final averaged result
% Set up figure
figure('Position', [750 350 1000 300])

% Display scaled, cropped BMode
subplot(1, 2, 1)
BModeCropped = BMode(axMin:axMax, latMin+settings.dlat:latMax-settings.dlat);
szB = size(BModeCropped);
BModeScaled = imresize(BModeCropped, [szB(1), 4*szB(2)]);

% Display, set aspect ratio
imagesc(BModeScaled), colormap(gca,'gray'), colorbar
title('B-mode')
%pbaspect([2*szB(2)/szB(1), 1, 1])

% Scale shear wave velocity estimate
subplot(1, 2, 2)
szS = size(avgSWVel);
SWScaled = imresize(avgSWVel, [szS(1), 4*szS(2)]);

% Scale image to m/s depending on settings
if scaleToVelocity == 1
    sz = size(SWScaled);
    distance = ones(sz(1), sz(2));
    distance = (2*4*0.308)*distance;  % mm / pixel
    time = 0.050 * SWScaled; % ms traveled
    SWScaled = distance ./ time;
end

% Display, set aspect ratio
imagesc(SWScaled); colorbar
title('Shear wave velocity map, averaged')
%pbaspect([2*szS(2)/szS(1), 1, 1])

%% Function definition
% Used to calculate shear wave velocity at a single point
function swvel = estimateLocalShearWaveVelocity(vel, ax, lat, direction, settings)
    % Threshold minimum allowed shear wave velocity by defining a maximum
    % lag time allowed between frames, e.g. max distance (time) between the 
    % peaks on the cross correlation plot
    minSWPxPerFrame = 0.1; % at Fs=10kHz, 6m/s, 0.308mm/px, the SWV=2px/fr
    maxFramesBetweenPeaks = (settings.dlat*2+1) / minSWPxPerFrame;
    
    % Get axial velocity as a function of time for two laterally-spaced 
    % reference points, refPt1 and refPt2
    refPt1 = vel(:, ax, lat-settings.dlat); 
    refPt2 = vel(:, ax, lat+settings.dlat);
    
    % Lowpass filter the time series; from settings struct
    if settings.do_lowpass
        refPt1 = filtfilt(settings.lowpass_B, settings.lowpass_A, refPt1);
        refPt2 = filtfilt(settings.lowpass_B, settings.lowpass_A, refPt2);
    end
    
    % Interpolate time series
    refPt1p = interp1(refPt1, 1:1/settings.interp_f:numel(refPt1), 'makima');
    refPt2p = interp1(refPt2, 1:1/settings.interp_f:numel(refPt2), 'makima');
    
    % Subtract mean from time series; from settings struct
    if settings.do_demean
        refPt1p = refPt1p - mean(refPt1p);
        refPt2p = refPt2p - mean(refPt2p);
    end
    
    % Hanning or Tukey window time series; from settings struct
    if settings.do_window
        win = hann(numel(refPt1p))';
        % win = tukeywin(numel(refPt1p), 0.25)';
        refPt1p = refPt1p .* win;
        refPt2p = refPt2p .* win;
    end
    
    % Cross-correlate, calculate lag times
    maxDelay = ceil(maxFramesBetweenPeaks*settings.interp_f);
    if strcmp(direction, 'LR')
        [cc, lags] = xcorr(refPt2p, refPt1p, maxDelay);
    elseif strcmp(direction, 'RL')
        [cc, lags] = xcorr(refPt1p, refPt2p, maxDelay);
    end
    [~, maxCCIndex] = max(cc);
    
    % Return SWV as pixel lag; scaling to m/s amplifies outliers
    swvel = abs(lags(maxCCIndex) / settings.interp_f);
    
%     % Plot some stuff for debugging
%     subplot(1, 2, 1)
%     plot(refPt1p); hold on; plot(refPt2p)
%     subplot(1, 2, 2)
%     plot(cc)
    
%     % Return shear wave velocity; doing this amplifies outliers...
%     distance = (2*settings.dlat*308); time = swvel*50; % um, us
%     swvel = distance/time; % um/us = m/s
    
%     % Close if using debug plots
%     close all
end
