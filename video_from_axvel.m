v = VideoWriter('SWE-axvel.avi');
v.FrameRate = 10;
open(v)

for i = 1:size(vFilt, 3)
    imagesc(imresize(medfilt2(vFilt(:,:,i), [5,5]), 5))
    axis off
    frame = getframe;
    writeVideo(v, frame.cdata);
end

close(v)