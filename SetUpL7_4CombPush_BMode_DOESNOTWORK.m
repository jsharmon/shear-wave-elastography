%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initial Version: John Pitre, 08/24/2017
% Last Updated: Jonah Harmon, 12/17/2019

% DESCRIPTION

% Implements an Unfocused Comb-push Ultrasound Shear Elastography imaging
% sequence for the Verasonics Vantage platform, using the L7-4 linear
% array transducer. Shear wave propagation is imaged using ultrafast plane
% wave acquisitions with optional multiple angle compounding.

% Adapted from:

%   Nordenfur, T. Comparison of pushing sequences for shear wave
%   elastography. Masters Thesis. KTH Royal Institute of Technology.
%   Stockholm, Sweden. 2013.

%   Song, P. et al. Comb-push Ultrasound Shear Elastography (CUSE): A Novel
%   Method for Two-dimensional Shear Elasticity Imaging of Soft Tissues.
%   IEEE Trans Med Imaging 2012 Sep; 31(9): 1821�1832.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Move to and activate directory; run with most recent update
vsrootv4

clear all

%% Specify P parameters.
% Imaging range in axial direction
P.startDepth = 10;                  % Acquisition depth in wavelengths
P.endDepth = 80;                    % Should be a multiple of 128 samples.

% For angle compounding; if P.na = 1, plane wave only
P.na = 3;                           % Set P.na = number of angles.
P.radToDeg = (pi/180);              % Convert between degrees and radians
if P.na == 1
    P.startAngle = 0;
else
    P.startAngle = -(P.radToDeg*4.0);   % Start at -4 degrees
end
P.dtheta = P.radToDeg*4.0;          % -4, 0, 4 degrees

% For saving IQ data for post processing
P.burstNum = 0;                        % Counting bursts to avoid overwriting files
P.numImagingEvents = 0;                % For tracking correct startEvent
P.currentLoc = 1;                      % Current imaging location
newDir = strrep(datestr(now),':','-'); % Current datetime on setup
newDir = strrep(newDir,' ','_');       % Remove spaces
P.initTime = newDir;                   % Directory for saving data, new each run

%% Specify push and SW tracking parameters.
% This may need varied based on stiffness of target; increase (e.g., reduce
% PRF) for soft targets to avoid striation artifact. Reduce (increase PRF)
% for stiffer materials to adequately sample. For PVA, try 50, for gelatin
% try 150.
swe.timeToNextAcq = 10;  
swe.nangles = 1;                     % Angle compounding during propagation
swe.imaging_prf = (1/((swe.timeToNextAcq)*10^(-6)))/swe.nangles;   % In Hz
swe.nimages = 100 * swe.nangles;     % Set number of tracking images

% For angle compounding, if desired
if swe.nangles == 1
    swe.startAngle = 0;
else
    swe.startAngle = -(P.radToDeg*4.0);   % Start at -4 degrees
end
swe.dtheta = P.radToDeg*4.0;          % -4, 0, 4 degrees

% Push parameters
swe.pushcycles = 1000;               % Push cycles - 1 us / 5 cycles
swe.nteeth = 3;                      % Number of "teeth" in comb push
swe.focus = 0;                       % In wavelengths; 0 for unfocused
swe.toothwidth = 12;                 % Number of elements in each push tooth

%% Specify system parameters.
Resource.Parameters.numTransmit = 128;     % Number of transmit channels.
Resource.Parameters.numRcvChannels = 128;  % Number of receive channels.
Resource.Parameters.speedOfSound = 1540;   % In m/sec
Resource.Parameters.connector = 2;         % Use L7-4 with second connector
Resource.Parameters.verbose = 2;
Resource.Parameters.simulateMode = 0;
% Resource.Parameters.simulateMode = 1;    % Forces simulate mode, even if hardware is present.
% Resource.Parameters.simulateMode = 2;    % Stops sequence and processes RcvData continuously.

%% Specify Trans structure array.
Trans.name = 'L7-4';
Trans.units = 'wavelengths';
Trans = computeTrans(Trans);
Trans.maxHighVoltage = 70; % Higher may cause L7-4 to disconnect during sequence

%% Specify PData structure array.
PData(1).PDelta = [Trans.spacing, 0, 0.5];
PData(1).Size(1) = ceil((P.endDepth-P.startDepth)/PData(1).PDelta(3));
PData(1).Size(2) = ceil((Trans.numelements*Trans.spacing)/PData(1).PDelta(1));
PData(1).Size(3) = 1;
PData(1).Origin = [-Trans.spacing*(Trans.numelements-1)/2,0,P.startDepth]; % x,y,z of upper lft crnr.

%% Specify Media object.
Media.MP(1,:) = [-10, 0, 100, 1];
Media.MP(2,:) = [ 10, 0, 100, 1];
Media.MP(3,:) = [ 50, 0, 100, 1];
Media.attenuation = -0.5;

%% Specify Resources, propagation imaging.
% RF Data
% Capture all RF data into one buffer, limit the number of transfers,
% maximize frame rate for burst imaging
Resource.RcvBuffer(1).datatype = 'int16';
Resource.RcvBuffer(1).rowsPerFrame = swe.nimages*(P.endDepth+P.startDepth)*4*4; % May need to increase if varying depth
Resource.RcvBuffer(1).colsPerFrame = 128;
Resource.RcvBuffer(1).numFrames = 1;

% IQ Data
Resource.InterBuffer(1).numFrames = 1;
Resource.InterBuffer(1).pagesPerFrame = swe.nimages; % One page for each recon acq

%% Specify Resources, default B-mode.
% Second RF buffer for B-mode imaging, for guidance before burst acq
Resource.RcvBuffer(2).datatype = 'int16';
Resource.RcvBuffer(2).rowsPerFrame = P.na*6144; % May need to increase if varying depth
Resource.RcvBuffer(2).colsPerFrame = 128;
Resource.RcvBuffer(2).numFrames = 1;

% For angle compounding for default B-mode
Resource.InterBuffer(2).numFrames = 1;   % Only need one frame

% Img Data
Resource.ImageBuffer(1).numFrames = 1;   % Only need one frame

% Display window
Resource.DisplayWindow(1).Title = 'L7-4, Unfocused Comb Push SWE';
Resource.DisplayWindow(1).pdelta = 0.3;
ScrnSize = get(0, 'ScreenSize');
DwWidth = ceil(PData(1).Size(2)*PData(1).PDelta(1)/Resource.DisplayWindow(1).pdelta);
DwHeight = ceil(PData(1).Size(1)*PData(1).PDelta(3)/Resource.DisplayWindow(1).pdelta);
Resource.DisplayWindow(1).Position = [250, (ScrnSize(4)-DwHeight-150)/2, DwWidth, DwHeight];
Resource.DisplayWindow(1).ReferencePt = [PData(1).Origin(1), 0, PData(1).Origin(3)];
Resource.DisplayWindow(1).numFrames = 2;
Resource.DisplayWindow(1).Colormap = gray(256);
Resource.DisplayWindow(1).AxesUnits = 'mm';

%% Specify Transmit waveform structure.
% Plane wave transmit
TW(1).type = 'parametric';
TW(1).Parameters = [Trans.frequency, 0.67, 2, 1];

% Push transmit
TW(2).type = 'parametric';
TW(2).Parameters = [Trans.frequency, 1, 2*swe.pushcycles, 1];

%% Specify TX structure array.
TX = repmat(struct('waveform', 1, ...
                   'Origin', [0.0,0.0,0.0], ...
                   'Apod', kaiser(Resource.Parameters.numTransmit,1)', ...
                   'focus', 0.0, ...
                   'Steer', [0.0,0.0], ...
                   'Delay', zeros(1,Trans.numelements)), 1, ...
                   swe.nangles+P.na+1);
               
%% Set event specific TX attributes.
% Propagation imaging TX
for n = 1:swe.nangles
    % Set up angled imaging transmits
    TX(n).Steer = [(swe.startAngle+(n-1)*swe.dtheta),0.0];
    TX(n).Delay = computeTXDelays(TX(n));
end

% Baseline B-mode TX
for n = swe.nangles+1:swe.nangles+P.na
    % Set up angled imaging transmits
    TX(n).Steer = [(P.startAngle+(n-swe.nangles-1)*P.dtheta),0.0];
    TX(n).Delay = computeTXDelays(TX(n));
end

% Push TX
TX(P.na+swe.nangles+1).waveform = 2; % Push waveform

% Compute apodization - from John
ngaps = Trans.numelements - swe.nteeth*swe.toothwidth;
gapwidth = floor(ngaps/(swe.nteeth - 1));
nelm_unused = Trans.numelements - swe.nteeth*swe.toothwidth - (swe.nteeth-1)*gapwidth;
first_tooth = 1 + floor(nelm_unused/2);
comb = zeros(1, Trans.numelements);
for tooth = 1:swe.nteeth
    tstart = first_tooth + (swe.toothwidth + gapwidth)*(tooth - 1);
    tend = tstart + swe.toothwidth - 1;
    comb(tstart:tend) = 1;
end
clear ngaps gapwidth nelm_unused first_tooth tstart tend tooth

% Set apodization
TX(P.na+swe.nangles+1).Apod = comb;

% Set transmit delays
TX(P.na+swe.nangles+1).Delay = computeTXDelays(TX(P.na+swe.nangles+1));

%% Specify TGC Waveform structure.
TGC.CntrlPts = [600, 600, 600, 600, 600, 600, 600, 600];
TGC.rangeMax = P.endDepth;
TGC.Waveform = computeTGCWaveform(TGC);

%% Specify high voltage TPC(5).
TPC(5).maxHighVoltage = Trans.maxHighVoltage;
TPC(5).hv = Trans.maxHighVoltage;

%% Specify Receive structure arrays.
% Note: The external function process_ucuse.m uses Receive(1) as a
% reference (evalin from base workspace). The assumption is that Receive(1)
% contains a normal plane wave receive structure for detection of the shear
% wave propagation.
maxAcqLength = ceil(sqrt(P.endDepth^2 + ((Trans.numelements-1)*Trans.spacing)^2));
Receive = repmat(struct('Apod', ones(1, Trans.numelements), ...
                        'startDepth', P.startDepth, ...
                        'endDepth', maxAcqLength, ...
                        'TGC', 1, ...
                        'bufnum', 1, ...
                        'framenum', 1, ...
                        'acqNum', 1, ...
                        'sampleMode', 'NS200BW', ...
                        'mode', 0, ...
                        'callMediaFunc', 1), 1, ...
                        swe.nimages+P.na*Resource.RcvBuffer(2).numFrames);
                    
%% Set event specific Receive attributes.
% Propagation imaging
for n = 1:swe.nimages
    Receive(n).acqNum = n;
end

% Default B-mode
for n = 1:Resource.RcvBuffer(2).numFrames
    for j = 1:P.na
        Receive(swe.nimages+P.na*(n-1)+j).bufnum = 2; % Second RF buffer
        Receive(swe.nimages+P.na*(n-1)+j).framenum = n;
        Receive(swe.nimages+P.na*(n-1)+j).acqNum = j;
    end
end

%% Specify Recon structure arrays.
Recon = repmat(struct('senscutoff', 0.6, ...
               'pdatanum', 1, ...
               'rcvBufFrame',1, ...
               'IntBufDest', [1,1], ...
               'ImgBufDest', [1,1], ...
               'RINums', 1:swe.nimages), 1, 2); % One for B-mode, one for prop
           
Recon(2).rcvBufFrame = -1;     % Grab last full frame to Recon
Recon(2).IntBufDest = [2, 1];  % Second buffer, first frame
Recon(2).ImgBufDest = [1, -1]; % Send to most recent frame
Recon(2).RINums = swe.nimages+1:swe.nimages+P.na;
% Recon(2).newFrameTimeout = 5000;

%% Define ReconInfo structures.
ReconInfo = repmat(struct('mode', 'accumIQ', ...
                          'txnum', 1, ...
                          'rcvnum', 1, ...
                          'pagenum', 1, ...
                          'regionnum', 1), 1, swe.nimages+P.na);

% Propagation imaging - do angle compounding if desired
pageNum = 0;
for n = 1:swe.nimages 
    % Generate counter for correct TX assignment
    if mod(n-1, swe.nangles) == 0
        pageNum = pageNum + 1;            % Increment page counter
        tx = 1;                           % Replace tx counter
        ReconInfo(n).mode = 'replaceIQ';  % Replace IQ on new page
    end
    
    % Specify tx, acquisition to process
    ReconInfo(n).txnum = tx;
    ReconInfo(n).rcvnum = n;
    
    % Increment tx counter
    tx = tx + 1;
    
    % Specify inter buffer page destination
    ReconInfo(n).pagenum = pageNum;
end

% Default B-mode - set up angle compounding
ReconInfo(swe.nimages+1).mode = 'replaceIQ';
for n = swe.nimages+1:swe.nimages+P.na
    ReconInfo(n).txnum = (n-swe.nimages)+swe.nangles;
    ReconInfo(n).rcvnum = n;
end
ReconInfo(n).mode = 'accumIQ_replaceIntensity';

%% Specify Process structure array.
% Make directory, save IQ
Process(1).classname = 'External';
Process(1).method = 'elastographyIQProcessing';
Process(1).Parameters = {'srcbuffer', 'inter', 'srcbufnum', 1, ... % Grab interbuffer
                         'srcframenum', 1, 'srcpagenum', 0, ... % 0 means get all pages
                         'dstbuffer', 'none'};
                     
pers = 20;
Process(2).classname = 'Image';
Process(2).method = 'imageDisplay';
Process(2).Parameters = {'imgbufnum',1,...   % Only one buffer
                         'framenum',-1,...   % Last processed frame
                         'pdatanum',1,...    % Only one PData
                         'pgain',1.0,...     % pgain is image processing gain
                         'reject',2,...      % Reject level 
                         'persistMethod','simple',...
                         'persistLevel',pers,...
                         'interpMethod','4pt',...  % Method of interp. (1=4pt)
                         'grainRemoval','none',...
                         'processMethod','none',...
                         'averageMethod','none',...
                         'compressMethod','power',...
                         'compressFactor',40,...
                         'mappingMethod','full',...
                         'display',1,...      % Display image after processing
                         'displayWindow',1};
                     
%% Specify SeqControl structure arrays.
% Change to profile 5 (high power)
SeqControl(1).command = 'setTPCProfile';
SeqControl(1).condition = 'immediate';
SeqControl(1).argument = 5;

% Let capacitor charge
SeqControl(2).command = 'noop';
SeqControl(2).argument = 500000;

% Sync
SeqControl(3).command = 'sync';

% Wait between push and first bmode frame
SeqControl(4).command = 'timeToNextAcq';
SeqControl(4).argument = 250;

% Wait between bmode frames, propagation
SeqControl(5).command = 'timeToNextAcq';
SeqControl(5).argument = swe.timeToNextAcq;

% Change to profile 1 (low power)
SeqControl(6).command = 'setTPCProfile';
SeqControl(6).condition = 'immediate';
SeqControl(6).argument = 1;

% Wait between bmode acqs, default imaging
SeqControl(7).command = 'timeToNextAcq';
SeqControl(7).argument = 1e6/(P.na*60); % 180 FPS

% Return control to Matlab
SeqControl(8).command = 'returnToMatlab';

% Jump for default imaging
SeqControl(9).command = 'jump';
SeqControl(9).argument = 3;

% Jump from SWV back to default imaging
SeqControl(10).command = 'jump';
SeqControl(10).condition = 'exitAfterJump';
SeqControl(10).argument = 1;

nsc = length(SeqControl) + 1;

%% Specify Event structure arrays - push and propagation imaging
n = 1;
Event(n).info = 'Switch to TPC 5';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = 1;
n = n + 1;

Event(n).info = 'Noop to charge capacitor';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = 2;
n = n + 1;

Event(n).info = 'Sync';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = 3;
n = n + 1;

% Push
Event(n).info = 'Push transmit';
Event(n).tx = P.na+swe.nangles+1;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = 4;
n = n + 1;

% Post-push imaging
for i = 1:swe.nimages
    Event(n).info = 'Propagation imaging';
    Event(n).tx = mod(i - 1, swe.nangles) + 1;
    Event(n).rcv = i;
    Event(n).recon = 0;
    Event(n).process = 0;
    Event(n).seqControl = 5;
    n = n + 1;
end
Event(n-1).seqControl = 0;

% Transfer frame to host
Event(n).info = 'Transfer frame to host';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = nsc;
SeqControl(nsc).command = 'transferToHost'; 
nsc = nsc + 1;
n = n + 1;

% Reconstruction
Event(n).info = 'Reconstruct IQ frame';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 1;
Event(n).process = 0;
Event(n).seqControl = 0;
n = n + 1;

% Save IQ or process and display SWV
Event(n).info = 'Save IQ for post-processing';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 1;
Event(n).seqControl = 0;
n = n + 1;

% Loop back to Event 1
Event(n).info = 'Jump to Event 1';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = 10;
n = n + 1;

% % For setting correct start event
% P.numImagingEvents = n-1;
% Resource.Parameters.startEvent = P.numImagingEvents+1;  % Default to pushing

% %% Specify Event structure arrays - default B-mode imaging
% Event(n).info = 'Switch to TPC 1';
% Event(n).tx = 0;
% Event(n).rcv = 0;
% Event(n).recon = 0;
% Event(n).process = 0;
% Event(n).seqControl = 6;
% n = n + 1;
% 
% % Noop to ensure switch is complete
% Event(n).info = 'Noop';
% Event(n).tx = 0;
% Event(n).rcv = 0;
% Event(n).recon = 0;
% Event(n).process = 0;
% Event(n).seqControl = 2;
% n = n + 1;
% 
% % Start imaging
% for i = 1:Resource.RcvBuffer(2).numFrames
%     Event(n).info = 'Sync';
%     Event(n).tx = 0;
%     Event(n).rcv = 0;
%     Event(n).recon = 0;
%     Event(n).process = 0;
%     Event(n).seqControl = 3;
%     n = n + 1;
% 
%     for j = 1:P.na      % Acquire frame
%         Event(n).info = 'Default B-mode transmit';
%         Event(n).tx = swe.nangles+j;
%         Event(n).rcv = swe.nimages+P.na*(i-1)+j;
%         Event(n).recon = 0;      % no reconstruction.
%         Event(n).process = 0;    % no processing
%         Event(n).seqControl = 7; % 180 Hz PRF
%         n = n+1;
%     end
%     Event(n-1).seqControl = [7, nsc]; % modify last acquisition Event's seqControl
%         SeqControl(nsc).command = 'transferToHost'; % transfer frame to host buffer
%         nsc = nsc+1;
% 
%     Event(n).info = 'Recon and process'; 
%     Event(n).tx = 0;         % no transmit
%     Event(n).rcv = 0;        % no rcv
%     Event(n).recon = 2;      % reconstruction
%     Event(n).process = 2;    % process
%     Event(n).seqControl = 8; % exit to matlab
%     n = n+1;
% end
% 
% % Loop back to Event 1
% Event(n).info = 'Jump to Event 1';
% Event(n).tx = 0;
% Event(n).rcv = 0;
% Event(n).recon = 0;
% Event(n).process = 0;
% Event(n).seqControl = 10;
% n = n + 1;

%% Specify UI elements
% Push, image, save IQ for post processing
UI(1).Control = {'UserB1','Style','VsPushButton','Label','Push, Save IQ',...
                 'Tag','SaveImageSequence'};
UI(1).Callback = text2cell('%BurstAcqCallbackSave');

% Push, image, display results
UI(2).Control = {'UserB2','Style','VsPushButton','Label','Push, Display',...
                 'Tag','SaveImageSequence'};
UI(2).Callback = text2cell('%BurstAcqCallbackDisplay');

% Move to next imaging site
UI(3).Control = {'UserB3','Style','VsPushButton','Label','Next location',...
                 'Tag','NextImagingLocation'};
UI(3).Callback = text2cell('%NextLocCallback');

%% Define external functions
EF(1).Function = text2cell('%ElastProc');

%% Conclude setup
% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 5; % Event sequence exits to Matlab every push/acq

% Save all the structures to a .mat file.
save('./MatFiles/L7-4CombPush');
return





%% UI(1) - Burst acquisition, save callback function
%BurstAcqCallbackSave
Resource = evalin('base', 'Resource');
P = evalin('base', 'P');
Resource.Parameters.startEvent = P.numImagingEvents+1;
assignin('base', 'Resource', Resource);

% Set Control command to update Event sequence
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'Event'};
assignin('base','Control', Control);

return
%BurstAcqCallbackSave

%% UI(2) - Burst acquisition, display callback function
%BurstAcqCallbackDisplay
return
%BurstAcqCallbackDisplay

%% UI(3) - Next location callback function
%NextLocCallback
P = evalin('base', 'P');
P.currentLoc = P.currentLoc + 1;
P.burstNum = 1;
disp('Ready to image next location')
assignin('base', 'P', P);

return
%NextLocCallback

%% EF(1) - Currently just saving IQ
%ElastProc
elastographyIQProcessing(IData, QData)
P = evalin('base', 'P'); % Get P info
Resource = evalin('base', 'Resource');
swe = evalin('base', 'swe');
rcv = evalin('base', 'Receive(1)'); % example plane wave Receive structure
c = evalin('base', 'Resource.Parameters.speedOfSound'); % sound speed
freq = evalin('base', 'Trans.frequency');

% Create unique folder for each run so users don't overwrite their files
% after running script again
P.burstSessionDir = ['Elast_BurstAcqSession_', P.initTime];
if ~exist(P.burstSessionDir, 'dir')
    mkdir(P.burstSessionDir);
end

cd(P.burstSessionDir)

P.singleLocationDir = sprintf('ImagingLocation%03d', P.currentLoc);
if ~exist(P.singleLocationDir, 'dir')
    mkdir(P.singleLocationDir);
end

cd ..

P.imageNum = 1;

% Create unique folder for each burst so users don't overwrite their files
% after running UI burst acq again
burstDir = ['./', P.burstSessionDir, '/', P.singleLocationDir, '/', sprintf('BurstAcq%03d/', P.burstNum)];
if ~exist(burstDir, 'dir')
    mkdir(burstDir);
end

% Notify user
disp('Saving IQ...')

% Save IQ frame to .mat file
IData = squeeze(IData); QData = squeeze(QData); % Remove extra dimension
save(['./', burstDir, sprintf('image%04d', P.imageNum)], 'IData', 'QData', 'swe', 'rcv', 'c', 'freq');
P.imageNum = P.imageNum + 1;

% Increment for new folder
P.burstNum = P.burstNum+1;

% Notify user
disp('Resuming imaging...')

assignin('base', 'P', P);
%ElastProc
