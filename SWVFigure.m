%% Produce images for SWV figures
% Start with axvel shear wave visualization; save images to
% organize/display in inkscape
xtix = {'0.5','1.0','1.5','2.0','2.5','3.0'};
ytix = {'6.16','12.3','18.5','24.6','30.8','37.0'};

% Raw
figure
imagesc(velExUnfiltered)
set(gca,'XTickLabel',xtix, 'YTickLabel',ytix); 
xlabel('Time (ms)'); ylabel('Lateral Position (mm)')
title('Axial Velocity, Raw')
saveas(gcf, 'RawAxVel.tif', 'tiffn')

% Raw, spectrum
figure
imagesc(abs(spectimRaw))
set(gca,'xtick',[],'ytick',[])
title('Axial Velocity, Raw, Spectrum')
saveas(gcf, 'RawAxVelSpectrum.tif', 'tiffn')

% Filtered, LR
figure
imagesc(velExFilteredLR)
set(gca,'XTickLabel',xtix, 'YTickLabel',ytix); 
xlabel('Time (ms)'); ylabel('Lateral Position (mm)')
title('Axial Velocity, Left-to-Right')
saveas(gcf, 'LRAxVel.tif', 'tiffn')

% Filtered, LR, spectrum
figure
imagesc(abs(spectIm1))
set(gca,'xtick',[],'ytick',[])
title('Axial Velocity, LR, Spectrum')
saveas(gcf, 'LRAxVelSpectrum.tif', 'tiffn')

% Filtered, RL
figure
imagesc(velExFilteredRL)
set(gca,'XTickLabel',xtix, 'YTickLabel',ytix); 
xlabel('Time (ms)'); ylabel('Lateral Position (mm)')
title('Axial Velocity, Right-to-Left')
saveas(gcf, 'RLAxVel.tif', 'tiffn')

% Filtered, RL, spectrum
figure
imagesc(abs(spectIm2))
set(gca,'xtick',[],'ytick',[])
title('Axial Velocity, RL, Spectrum')
saveas(gcf, 'RLAxVelSpectrum.tif', 'tiffn')

%% Shear wave velocity maps, single direction and composite
caxisscale = 28;
lattix = {'6.16','12.3','18.5','24.6','30.8'};
axtix = {'0.77','1.54','2.31','3.08','3.85','4.62','5.39','6.16','6.93'};

% Shear wave velocity map, single direction
figure
imagesc(swVelLR), h = colorbar; ylabel(h, 'Relative Shear Wave Velocity')
caxis([0 caxisscale])
set(gca,'XTickLabel',lattix, 'YTickLabel',axtix)
xlabel('Lateral Position (mm)'); ylabel('Axial Position (mm)')
title('Shear Wave Velocity, Left-to-Right')
saveas(gcf, 'LRSWV.tif', 'tiffn')

% Shear wave velocity map, RL
figure
imagesc(swVelRL), h = colorbar; ylabel(h, 'Relative Shear Wave Velocity')
caxis([0 caxisscale])
set(gca,'XTickLabel',lattix,'YTickLabel',axtix)
xlabel('Lateral Position (mm)'); ylabel('Axial Position (mm)')
title('Shear Wave Velocity, Right-to-Left')
saveas(gcf, 'RLSWV.tif', 'tiffn')

% Shear wave velocity map, averaged
figure
imagesc(avgSWVel), h = colorbar; ylabel(h, 'Relative Shear Wave Velocity')
caxis([0 caxisscale])
set(gca,'XTickLabel',lattix,'YTickLabel',axtix)
xlabel('Lateral Position (mm)'); ylabel('Axial Position (mm)')
title('Shear Wave Velocity, Composite')
saveas(gcf, 'CompositeSWV.tif', 'tiffn')

%% Full examples, with B-mode, SWV, and padded png for overlay
% B-mode
lattixB = {'6.16','12.3','18.5','24.6','30.8','37.0'};
axtixB = {'1.54','3.08','4.62','6.16','7.70','9.24','10.8'};

figure
imagesc(BMode(1:140, :)), colormap(gca,'gray')
rectangle('Position', [latMin, axMin, latMax-latMin, axMax-axMin], 'EdgeColor', 'r') 
title('B-mode'); xlabel('Lateral Position (mm)'); ylabel('Axial Position (mm)')
set(gca,'XTickLabel',lattixB,'YTickLabel',axtixB)
saveas(gcf, 'BMode-ROI.tif', 'tiffn')

% Shear wave velocity map, averaged, scaled to fit ROI; combine in post
figure
sz = size(BMode(1:140,:));
swmap = zeros(sz(1), sz(2));
swmap(axMin+6:axMax-6, latMin+4:latMax-4) = avgSWVel;
imagesc(swmap); h = colorbar; ylabel(h, 'Relative Shear Wave Velocity')
caxis([0 caxisscale])
set(gca,'xtick',[],'ytick',[])
saveas(gcf, 'SWV.png')

% Shear wave velocity map, averaged, scaled to fit ROI; combine in post
figure
swmap = zeros(sz(1), sz(2));
swmap(axMin+6:axMax-6, latMin+4:latMax-4) = avgSWVel;
imagesc(swmap);
caxis([0 caxisscale])
set(gca,'xtick',[],'ytick',[])
saveas(gcf, 'SWV.png')
