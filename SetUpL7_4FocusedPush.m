%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Last Updated: Jonah Harmon, 12/10/2019

% DESCRIPTION

% Implements SWE with a single focused push.

% Adapted from:

%   Nordenfur, T. Comparison of pushing sequences for shear wave
%   elastography. Masters Thesis. KTH Royal Institute of Technology.
%   Stockholm, Sweden. 2013.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Move to and activate directory; run with most recent update
vsrootv4

clear all

%% Specify P parameters.
P.startDepth = 10;                  % Acquisition depth in wavelengths
P.endDepth = 80;                    % Should be a multiple of 128 samples.
P.na = 3;                           % Set P.na = number of angles.
P.radToDeg = (pi/180);              % Convert between degrees and radians
P.startAngle = -(P.radToDeg*4.0);   % Start at -4 degrees
P.dtheta = P.radToDeg*4.0;          % -4, 0, 4 degrees
P.burstNum = 0;                     % Counting bursts to avoid overwriting files
P.numImagingEvents = 0;             % For tracking correct startEvent
P.currentLoc = 1;                   % Current imaging location

newDir = strrep(datestr(now),':','-'); % Current datetime on setup
newDir = strrep(newDir,' ','_');       % Remove spaces
P.initTime = newDir;                   % Directory for saving data, new each run

%% Specify push parameters.
swe.imaging_prf = 30000;                   % PRF of transmits, effective FR * 3 (Hz)
swe.postpush_imgtime = 30e-3;               % Imaging time after push, seconds
swe.nangles = P.na;                        % Number of imaging angles
swe.angles = linspace(-4, 4, swe.nangles); % Imaging transmit angles
swe.pushcycles = 1000;                     % Push cycles - 1 us / 5 cycles
swe.focus = 40;                            % In wavelengths; 0 for unfocused
swe.pushElements = 32;                     % Number of elements involved in push
swe.pushCenter = 20;                       % Element around which to center push elements

% Calculate number of pulses/RF acqs for post-push imaging
swe.shrwave_imaging_pulses = ceil(swe.postpush_imgtime*swe.imaging_prf);
swe.nimages = 1+swe.shrwave_imaging_pulses;
% 1+swe.shrwave_imaging_pulses; Use this, plus 30000 and 30 e -3 if you need to crash Matlab to delete burstacq folders

%% Specify system parameters.
Resource.Parameters.numTransmit = 128;     % number of transmit channels.
Resource.Parameters.numRcvChannels = 128;  % number of receive channels.
Resource.Parameters.speedOfSound = 1540;   % set speed of sound in m/sec before calling computeTrans
Resource.Parameters.connector = 2;         % Use L7-4 with second connector
Resource.Parameters.verbose = 2;
Resource.Parameters.simulateMode = 0;
% Resource.Parameters.simulateMode = 1;  % forces simulate mode, even if hardware is present.
% Resource.Parameters.simulateMode = 2;  % stops sequence and processes RcvData continuously.

%% Specify Trans structure array.
Trans.name = 'L7-4';
Trans.units = 'wavelengths';
Trans = computeTrans(Trans);
Trans.maxHighVoltage = 70; % Both sources they ran at 90 V...

%% Specify PData structure array.
PData(1).PDelta = [Trans.spacing, 0, 0.5];
PData(1).Size(1) = ceil((P.endDepth-P.startDepth)/PData(1).PDelta(3));
PData(1).Size(2) = ceil((Trans.numelements*Trans.spacing)/PData(1).PDelta(1));
PData(1).Size(3) = 1;
PData(1).Origin = [-Trans.spacing*(Trans.numelements-1)/2,0,P.startDepth]; % x,y,z of upper lft crnr.

%% Specify Media object.
Media.MP(1,:) = [-10, 0, 100, 1];
Media.MP(2,:) = [ 10, 0, 100, 1];
Media.MP(3,:) = [ 50, 0, 100, 1];
Media.attenuation = -0.5;

%% Specify Resources.
% RF Data
% Capture all RF data into one buffer, limit the number of transfers,
% maximize frame rate for burst imaging
Resource.RcvBuffer(1).datatype = 'int16';
Resource.RcvBuffer(1).rowsPerFrame = swe.nimages*(P.endDepth+P.startDepth)*4*4; % May need to increase if varying depth
Resource.RcvBuffer(1).colsPerFrame = 128;
Resource.RcvBuffer(1).numFrames = 1;

% IQ Data
Resource.InterBuffer(1).numFrames = 1;
Resource.InterBuffer(1).pagesPerFrame = swe.nimages/P.na; % One page for each recon acq

% Img Data
Resource.ImageBuffer(1).numFrames = 1;   % Only 1 frame required

% Display window - not used
Resource.DisplayWindow(1).Title = 'L7-4, Focused Push SWE';
Resource.DisplayWindow(1).pdelta = 0.3;
ScrnSize = get(0, 'ScreenSize');
DwWidth = ceil(PData(1).Size(2)*PData(1).PDelta(1)/Resource.DisplayWindow(1).pdelta);
DwHeight = ceil(PData(1).Size(1)*PData(1).PDelta(3)/Resource.DisplayWindow(1).pdelta);
Resource.DisplayWindow(1).Position = [250, (ScrnSize(4)-DwHeight-150)/2, DwWidth, DwHeight];
Resource.DisplayWindow(1).ReferencePt = [PData(1).Origin(1), 0, PData(1).Origin(3)];
Resource.DisplayWindow(1).numFrames = 2;
Resource.DisplayWindow(1).Colormap = gray(256);
Resource.DisplayWindow(1).AxesUnits = 'mm';

%% Specify Transmit waveform structure.
% Plane wave transmit
TW(1).type = 'parametric';
TW(1).Parameters = [Trans.frequency, 0.67, 2, 1];

% Push transmit
TW(2).type = 'parametric';
TW(2).Parameters = [Trans.frequency, 1, 2*swe.pushcycles, 1];

%% Specify TX structure array.
TX = repmat(struct('waveform', 1, ...
                   'Origin', [0.0,0.0,0.0], ...
                   'Apod', kaiser(Resource.Parameters.numTransmit,1)', ...
                   'focus', 0.0, ...
                   'Steer', [0.0,0.0], ...
                   'Delay', zeros(1,Trans.numelements)), 1, P.na+1);
               
%% Set event specific TX attributes.
% Imaging TX
for n = 1:P.na
    % Set up angled imaging transmits
    TX(n).Steer = [(P.startAngle+(n-1)*P.dtheta),0.0];
    TX(n).Delay = computeTXDelays(TX(n));
end

% Push TX
TX(P.na+1).waveform = 2; % Push waveform

% Set apodization, origin, focus
TX(P.na+1).Apod = zeros(1, Trans.numelements);
TX(P.na+1).Apod(swe.pushCenter-swe.pushElements/2:swe.pushCenter+swe.pushElements/2) = 1;
TX(P.na+1).Origin = [Trans.ElementPos(swe.pushCenter), 0, 0];
TX(P.na+1).focus = swe.focus;

% Set transmit delays
TX(P.na+1).Delay = computeTXDelays(TX(P.na+1));

%% Specify TGC Waveform structure.
TGC.CntrlPts = [500, 590, 650, 710, 770, 830, 890, 950];
TGC.rangeMax = P.endDepth;
TGC.Waveform = computeTGCWaveform(TGC);

%% Specify high voltage TPC(5).
TPC(5).maxHighVoltage = Trans.maxHighVoltage;

%% Specify Receive structure arrays.
% Note: The external function process_ucuse.m uses Receive(1) as a
% reference (evalin from base workspace). The assumption is that Receive(1)
% contains a normal plane wave receive structure for detection of the shear
% wave propagation.
maxAcqLength = ceil(sqrt(P.endDepth^2 + ((Trans.numelements-1)*Trans.spacing)^2));
Receive = repmat(struct('Apod', ones(1, Trans.numelements), ...
                        'startDepth', P.startDepth, ...
                        'endDepth', maxAcqLength, ...
                        'TGC', 1, ...
                        'bufnum', 1, ...
                        'framenum', 1, ...
                        'acqNum', 1, ...
                        'sampleMode', 'NS200BW', ...
                        'mode', 0, ...
                        'callMediaFunc', 1), 1, swe.nimages);
                    
%% Set event specific Receive attributes.
for n = 1:swe.nimages
    Receive(n).acqNum = n;
end

%% Specify Recon structure arrays.
Recon = struct('senscutoff', 0.6, ...
               'pdatanum', 1, ...
               'rcvBufFrame',1, ...
               'IntBufDest', [1,1], ...
               'ImgBufDest', [1,1], ...
               'RINums', 1:swe.nimages);

%% Define ReconInfo structures.
ReconInfo = repmat(struct('mode', 'accumIQ', ...
                          'txnum', 1, ...
                          'rcvnum', 1, ...
                          'pagenum', 1, ...
                          'regionnum', 1), 1, swe.nimages);

% Loop through each acquisition, compound angles
pageNum = 0;
for n = 1:swe.nimages 
    % Generate counter for correct TX assignment
    if mod(n-1, P.na) == 0
        pageNum = pageNum + 1;            % Increment page counter
        tx = 1;                           % Replace tx counter
        ReconInfo(n).mode = 'replaceIQ';  % Replace IQ on new page
    end
    
    % Specify tx, acquisition to process
    ReconInfo(n).txnum = tx;
    ReconInfo(n).rcvnum = n;
    
    % Increment tx counter
    tx = tx + 1;
    
    % Specify inter buffer page destination
    ReconInfo(n).pagenum = pageNum;
end

%% Specify Process structure array.
% Make directory, save IQ
Process(1).classname = 'External';
Process(1).method = 'elastographyIQProcessing';
Process(1).Parameters = {'srcbuffer', 'inter', 'srcbufnum', 1, ... % Grab interbuffer
                         'srcframenum', 1, 'srcpagenum', 0, ... % 0 means get all pages
                         'dstbuffer', 'none'};
                     
%% Specify SeqControl structure arrays.
% Change to profile 5 (high power)
SeqControl(1).command = 'setTPCProfile';
SeqControl(1).condition = 'immediate';
SeqControl(1).argument = 5;

% Let capacitor charge
SeqControl(2).command = 'noop';
SeqControl(2).argument = 500000;

% Sync
SeqControl(3).command = 'sync';

% Wait between bmode frames
SeqControl(4).command = 'timeToNextAcq';
SeqControl(4).argument = 50; % Convert PRF to microsecond wait time

% Wait between push and first bmode frame
SeqControl(5).command = 'timeToNextAcq';
SeqControl(5).argument = 250;

% Return control to Matlab
SeqControl(6).command = 'returnToMatlab';

% Jump
SeqControl(7).command = 'jump';
SeqControl(7).argument = 1;    % Can probably jump to 2 but just in case...

nsc = length(SeqControl) + 1;

%% Specify Event structure arrays.
n = 1;
Event(n).info = 'Switch to TPC 5';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = 1;
n = n + 1;

Event(n).info = 'Noop to charge capacitor';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = 2;
n = n + 1;

Event(n).info = 'Sync';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = 3;
n = n + 1;

% Push
Event(n).info = 'Push transmit';
Event(n).tx = P.na+1;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = 5;
n = n + 1;

% Post-push imaging
for i = 1:swe.nimages   % 10 KHz / 3 effective FR
    Event(n).info = 'Propagation imaging';
    Event(n).tx = mod(i - 1, swe.nangles) + 1;
    Event(n).rcv = i;
    Event(n).recon = 0;
    Event(n).process = 0;
    Event(n).seqControl = 4;
    n = n + 1;
end
Event(n - 1).seqControl = 0;

% Transfer data
Event(n).info = 'Transfer frame to host';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = nsc;
SeqControl(nsc).command = 'transferToHost';
nsc = nsc + 1;
n = n + 1;

% Reconstruction
Event(n).info = 'Reconstruct IQ frames';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 1;
Event(n).process = 0;
Event(n).seqControl = 0;
n = n + 1;

% Shear wave velocity processing
Event(n).info = 'Save IQ for post-processing';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 1;
Event(n).seqControl = 0;
n = n + 1;

% Loop back
Event(n).info = 'Jump';
Event(n).tx = 0;
Event(n).rcv = 0;
Event(n).recon = 0;
Event(n).process = 0;
Event(n).seqControl = 7;
n = n + 1;

%% Define external functions
EF(1).Function = text2cell('%ElastProc');

%% Conclude setup
% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 1; % Event sequence exits to Matlab every push/acq

% Save all the structures to a .mat file.
save('./MatFiles/L7-4FocusedPush');
return





%% EF(1) - Currently just saving IQ
%ElastProc
elastographyIQProcessing(IData, QData)
P = evalin('base', 'P'); % Get P info
Resource = evalin('base', 'Resource');
swe = evalin('base', 'swe');
rcv = evalin('base', 'Receive(1)'); % example plane wave Receive structure
c = evalin('base', 'Resource.Parameters.speedOfSound'); % sound speed
freq = evalin('base', 'Trans.frequency');

% Create unique folder for each run so users don't overwrite their files
% after running script again
P.burstSessionDir = ['Elast_BurstAcqSession_', P.initTime];
if ~exist(P.burstSessionDir, 'dir')
    mkdir(P.burstSessionDir);
end

cd(P.burstSessionDir)

P.singleLocationDir = sprintf('ImagingLocation%03d', P.currentLoc);
if ~exist(P.singleLocationDir, 'dir')
    mkdir(P.singleLocationDir);
end

cd ..

P.imageNum = 1;

% Create unique folder for each burst so users don't overwrite their files
% after running UI burst acq again
burstDir = ['./', P.burstSessionDir, '/', P.singleLocationDir, '/', sprintf('BurstAcq%03d/', P.burstNum)];
if ~exist(burstDir, 'dir')
    mkdir(burstDir);
end

% Notify user
disp('Saving IQ...')

% Save IQ frame to .mat file
IData = squeeze(IData); QData = squeeze(QData); % Remove extra dimension
save(['./', burstDir, sprintf('image%04d', P.imageNum)], 'IData', 'QData', 'swe', 'rcv', 'c', 'freq');
P.imageNum = P.imageNum + 1;

% Increment for new folder
P.burstNum = P.burstNum+1;

assignin('base', 'P', P);
%ElastProc
